package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

var (
	teamID    string
	startDate string
	startTime string
	endDate   string
	endTime   string

	// BaseURL ...
	BaseURL = "https://extra-life.org/api/"

	// ISO8601 ...
	ISO8601 = "2006-01-02T15:04:05.999999999-0700"
)

func init() {
	flag.StringVar(&teamID, "t", "", "The ID for a team you want to grab the data from")
	flag.StringVar(&startDate, "sd", "", "The Start Date in UTC in the format YYYY-MM-DD")
	flag.StringVar(&startTime, "st", "", "The Start Time in UTC in the format HH:MM")
	flag.StringVar(&endDate, "ed", "", "The end Date in UTC in the format YYYY-MM-DD")
	flag.StringVar(&endTime, "et", "", "The end Time in UTC in the format HH:MM")
}

// Team ...
type Team struct {
	AvatarImageURL  string  `json:"avatarImageURL"`
	CreatedDateUTC  string  `json:"createdDateUTC"`
	EventID         int64   `json:"eventID"`
	EventName       string  `json:"eventName"`
	FundraisingGoal float64 `json:"fundraisingGoal"`
	Name            string  `json:"name"`
	NumDonations    int     `json:"numDonations"`
	SumDonations    float64 `json:"sumDonations"`
	TeamID          int64   `json:"teamID"`

	CreatedDate time.Time
}

// Participant ...
type Participant struct {
	AvatarImageURL  string  `json:"avatarImageURL"`
	CampaignDate    string  `json:"campaignDate"`
	CampaignName    string  `json:"campaignName"`
	CreatedDateUTC  string  `json:"createdDateUTC"`
	DisplayName     string  `json:"displayName"`
	EventID         int64   `json:"eventID"`
	EventName       string  `json:"eventName"`
	FundraisingGoal float64 `json:"fundraisingGoal"`
	IsTeamCaptain   bool    `json:"isTeamCaptain"`
	NumDonations    int     `json:"numDonations"`
	ParticipantID   int64   `json:"participantID"`
	SumDonations    float64 `json:"sumDonations"`
	TeamID          int64   `json:"teamID"`
	TeamName        string  `json:"teamName"`

	CreatedDate time.Time
}

// Participants ...
type Participants []*Participant

// Find ...
func (p *Participants) Find(id int64) *Participant {
	for _, v := range *p {
		if v.ParticipantID == id {
			return v
		}
	}

	return nil
}

// Donation ...
type Donation struct {
	Amount         float32 `json:"amount"`
	AvatarImageURL string  `json:"avatarImageURL"`
	CreatedDateUTC string  `json:"createdDateUTC"`
	DisplayName    string  `json:"displayName"`
	DonorID        string  `json:"donorID"`
	Message        string  `json:"message"`
	ParticipantID  int64   `json:"participantID"`
	TeamID         int     `json:"teamID"`

	CreatedDate time.Time
}

// Donations ...
type Donations []*Donation

func main() {
	flag.Parse()

	if strings.EqualFold(teamID, "") {
		log.Println("Missing teamID flag")
		return
	}
	if strings.EqualFold(startDate, "") {
		log.Println("Missing startDate flag")
		return
	}
	if strings.EqualFold(startTime, "") {
		log.Println("Missing startTime flag")
		return
	}
	if strings.EqualFold(endDate, "") {
		log.Println("Missing endDate flag")
		return
	}
	if strings.EqualFold(endTime, "") {
		log.Println("Missing endTime flag")
		return
	}

	startDateTime, err := time.Parse("2006-01-02 15:04", fmt.Sprintf("%s %s", startDate, startTime))

	if err != nil {
		log.Println("An error occured during parsing startDateTime:", err)
		return
	}

	endDateTime, err := time.Parse("2006-01-02 15:04", fmt.Sprintf("%s %s", endDate, endTime))
	if err != nil {
		log.Println("An error occured during parsing endDateTime:", err)
		return
	}
	team := requestTeam(teamID)
	participants := requestTeamParticipants(team)

	allDonations, _ := handleDonations(team, startDateTime, endDateTime)

	log.Println(len(allDonations))

	err = writeDonationsToFile(&allDonations, &participants, "allDonations.csv", ";")
	if err != nil {
		log.Println("An error occured during creationg of file allDonations:", err)
	}

}

func handleDonations(team Team, startTime, endTime time.Time) (Donations, map[int64]Donations) {
	var allDonations Donations
	donationsPerParticipant := make(map[int64]Donations)
	var err error

	limit := 100
	offset := 1

	passedEndDate := false

	for !passedEndDate {
		donations := requestTeamDonations(team, offset, limit)
		for _, donation := range donations {
			donation.CreatedDate, err = time.Parse(ISO8601, donation.CreatedDateUTC)

			if err != nil {
				log.Println("An error occured during parsing of CreatedDateUTC ", donation.CreatedDateUTC, ":", err)
			}
			if donation.CreatedDate.Before(endTime) || donation.CreatedDate.Equal(endTime) {
				if donation.CreatedDate.Before(startTime) {
					passedEndDate = true
					break
				}

				allDonations = append(allDonations, donation)
				donationsPerParticipant[donation.ParticipantID] = append(donationsPerParticipant[donation.ParticipantID], donation)
			}
		}
		offset += len(donations)
	}

	return allDonations, donationsPerParticipant
}

func sendRequestAndUnmarshal(url string, object interface{}) error {

	resp, err := http.Get(url)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	err = json.Unmarshal(body, object)
	if err != nil {
		log.Println(url)
		return err
	}

	return nil
}

func requestTeam(teamID string) Team {
	log.Printf("Requesting team with id %s\n", teamID)

	var team Team
	err := sendRequestAndUnmarshal(fmt.Sprintf("%steams/%s", BaseURL, teamID), &team)
	if err != nil {
		log.Println("Something went wrong during requesting team donations:", err)
		return Team{}
	}

	return team
}

func requestTeamDonations(team Team, offset, limit int) Donations {
	log.Printf("Requesting Donations for team %s with offset %d and limit %d\n", team.Name, offset, limit)

	var donations Donations
	err := sendRequestAndUnmarshal(fmt.Sprintf("%steams/%d/donations?limit=%d&offset=%d", BaseURL, team.TeamID, limit, offset), &donations)
	if err != nil {
		log.Println("Something went wrong during requesting team donations:", err)
		return nil
	}

	return donations
}

func requestTeamParticipants(team Team) Participants {
	log.Printf("Requesting Participants for team %s\n", team.Name)

	var participants Participants
	err := sendRequestAndUnmarshal(fmt.Sprintf("%steams/%d/participants", BaseURL, team.TeamID), &participants)
	if err != nil {
		log.Println("Something went wrong during requesting team participants:", err)
		return nil
	}

	return participants
}

func writeDonationsToFile(donations *Donations, participants *Participants, fileName, csvValue string) error {
	f, err := os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}
	defer f.Close()

	if _, err = f.WriteString(fmt.Sprintf("Donator%sAmount%sRecipient\n", csvValue, csvValue)); err != nil {
		return err
	}
	for _, donation := range *donations {
		participant := participants.Find(donation.ParticipantID)
		if participant == nil {
			continue
		}

		if _, err = f.WriteString(fmt.Sprintf("%s%s%.2f%s%s\n", donation.DisplayName, csvValue, donation.Amount, csvValue, participant.DisplayName)); err != nil {
			return err
		}
	}

	if _, err = f.WriteString("\n"); err != nil {
		return err
	}

	return err
}
